export default class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	renderAttribute({ name, value }) {
		return `${name}="${value}"`;
	}

	render() {
		if (this.attribute == null || this.attribute == undefined) {
			return `<${this.tagName}>${this.children}</${this.tagName}>`;
		} else {
			return `<${this.tagName} ${this.renderAttribute(this.attribute)} />`;
		}
	}
}
